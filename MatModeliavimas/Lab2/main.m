clear;
close all;
%RLSE
t_C=[13, 14, 17, 18, 19, 15, 13, 31, 32, 29, 27];
t_F=[55, 58, 63, 65, 66, 59, 56, 87, 90, 85, 81];
NA=length(t_C);
x=zeros(2,1);
P=100*eye(2,2);
for k=1:NA,
    A(k, :) = [t_F(k), 1];
    B(k, :) = [t_C(k)];
    [x,K,P]=rlse_online(A(k,:),B(k,:),x,P);
end
fprintf('k RLSE vertes = %f  %f\n',x(1),x(2));
%pinv
%t_C = k1 t_F + 1 k2
%k1=
k_n = pinv(A)*B;
fprintf('k pinv() vertes = %f  %f\n',k_n(1),k_n(2));
fprintf('k tikros vertes = %f  %f\n',5/9,-(17+7/9))

%stiprinimo matricos priklausomybes
P_n = [0:0.1:1.5];

for n = 1:length(P_n),
    P=P_n(n)*eye(2,2);
    for k=1:NA,
    A(k, :) = [t_F(k), 1];
    B(k, :) = [t_C(k)];
    [x,K,P]=rlse_online(A(k,:),B(k,:),x,P);
    end
    k1_n(n)=x(1);
    k2_n(n)=x(2);
end
k1_n0 = 5/9*ones(1, length(P_n));
k2_n0 = -(17+7/9)*ones(1, length(P_n));
plot(P_n, k1_n, P_n, k2_n, P_n, k1_n0, P_n, k2_n0);
grid on;
xlabel('n');
ylabel('k');